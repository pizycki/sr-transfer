package ik.BankSrv.Contracts;

import java.io.FileNotFoundException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface IClientAccountSrv {
	
    @WebMethod
    Double checkBalance(String accountNumber) throws FileNotFoundException;
    
    @WebMethod
    boolean transfer(String fromAccountNumber, String toAccountNumber, Double amount);
    
    @WebMethod
    boolean logIn(String accountNumber, String accountPassword);
}
