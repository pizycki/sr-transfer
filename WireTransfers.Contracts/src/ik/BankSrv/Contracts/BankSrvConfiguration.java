package ik.BankSrv.Contracts;

public interface BankSrvConfiguration {
	String getBankID();

	String getAddress();

	int getWireTransferSrvPort();

	int getClientAccountSrvPort();
	
}
