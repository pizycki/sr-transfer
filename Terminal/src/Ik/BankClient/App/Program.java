package Ik.BankClient.App;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import ik.BankSrv.Contracts.IClientAccountSrv;

public class Program {
    
    public static void main(String[] args) throws Exception {
    	URL url = new URL("http://localhost:5557/ws/client?wsdl");
    	 
        //1st argument service URI, refer to wsdl document above
	//2nd argument is service name, refer to wsdl document above
        QName qname = new QName("http://Mock/", "ClientAccountWebServiceService");
 
        Service service = Service.create(url, qname);
 
        IClientAccountSrv client = service.getPort(IClientAccountSrv.class);
        
 
        System.out.println(client.checkBalance("test").toString());
    }    
}
