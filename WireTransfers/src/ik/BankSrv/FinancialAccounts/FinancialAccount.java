package ik.BankSrv.FinancialAccounts;

public class FinancialAccount {
    
    public String accountNumber;
    public Double balance;

    public FinancialAccount(){
    	
    }
    
    public FinancialAccount(String accountNumber, Double balance) {
        this.accountNumber = accountNumber;
        this.balance = balance;
    }
    
    //Getters & Setters
    public String getAccountNumber() {
		return accountNumber;
	}
    
    public Double getBalance() {
		return balance;
	}
    
    public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
    
    public void setBalance(Double balance) {
		this.balance = balance;
	}
}
