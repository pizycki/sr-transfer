package ik.BankSrv.FinancialAccounts;

import java.io.Serializable;
import java.util.Set;

public class FinancialAccountSet implements Serializable {
	public Set<FinancialAccount> financialAccountsSet;
	
	public FinancialAccountSet(){
		
	}
	
	public FinancialAccountSet(Set<FinancialAccount> accounts){
		this.financialAccountsSet = accounts;
	}

}
