package ik.BankSrv.FinancialAccounts;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

import ik.BankSrv.FinancialAccounts.FinancialAccountSet;
import ik.BankSrv.Utilities.Serializer;

public class FinancialAccountSrv {
	
    public FinancialAccountSrv(){
    	prepareMockAccounts();
    }
    
    public boolean CreateAccount(String newAccountNumber, Double initBalance) {
    	FinancialAccount account = new FinancialAccount(newAccountNumber, initBalance);
    	
    	try {
			Serializer.serialize(account, ACCOUNT_DATA_FILEPATH);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
    
    public boolean prepareMockAccounts() {
		FinancialAccount account1 = new FinancialAccount("123456", 123.0);
		FinancialAccount account2 = new FinancialAccount("234567", 2345.0);
		
		Set<FinancialAccount> accounts = new HashSet<FinancialAccount>();
		accounts.add(account1);
		accounts.add(account2);
		
		FinancialAccountSet accountSet = new FinancialAccountSet(accounts);
		
		try {
			Serializer.serialize(accountSet, ACCOUNT_DATA_FILEPATH);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
    
    public Double getBalance(String accountNumber) throws FileNotFoundException{
    	
    	FinancialAccount acc = findAccountByAccountNumer(accountNumber);
    	
		return acc.getBalance();
	}
    
    public boolean changeBalance(String accountNumber, Double amount) throws FileNotFoundException{
    	
    	try {
    		FinancialAccount account = findAccountByAccountNumer(accountNumber);
    		Double temp = account.getBalance();
    	
    		account.setBalance(temp + amount);
    	
    		return true;
		} catch (Exception e) {
			return false;
		}
    }
    
    public FinancialAccount findAccountByAccountNumer(String AccountNumber) throws FileNotFoundException {
    	
    	FinancialAccountSet accountSet = Serializer.deserialize(ACCOUNT_DATA_FILEPATH, FinancialAccountSet.class);
		FinancialAccount account = new FinancialAccount();
		
		for (FinancialAccount acc : accountSet.financialAccountsSet) {
			if (acc.getAccountNumber().equals(AccountNumber)) {
				account = acc;
			}
		}
		
		return account;
	}
    
    public static final String ACCOUNT_DATA_FILEPATH = "AccountData.data"; 
}
