/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ik.BankSrv.ClientAccountService;

import java.io.FileNotFoundException;

import ik.BankSrv.Contracts.IClientAccountSrv;
import ik.BankSrv.FinancialAccounts.FinancialAccountSrv;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService(endpointInterface = "ik.BankSrv.Contracts.IClientAccountSrv")
public class ClientAccountSrv implements IClientAccountSrv{
	
    private void init() {
		openEndpoints();
		this.financialAccountSrv = new FinancialAccountSrv();		
	}
    
    private void openEndpoints() {
		String publishAddress = configuration.getPublishAddress() + ":" + configuration.getPublishPort() + "/ws/";
		String[] endPointNames = configuration.getEndpointName();
		for (String endPointName : endPointNames) {
			String fullPublishAddress = publishAddress + endPointName;
			Endpoint.publish(fullPublishAddress, this);
		}
	}
    
    public Double checkBalance(String accountNumber) throws FileNotFoundException {
        return financialAccountSrv.getBalance(accountNumber);
    }
    
    public ClientAccountSrv() {
	}
    
    public ClientAccountSrv(ClientAccountSrvConfiguration configuration){
    	this.configuration = configuration;
    	
    	init();
    }
    
	private ClientAccountSrvConfiguration configuration;
	private FinancialAccountSrv financialAccountSrv;
}
