package ik.BankSrv.ClientAccountService;

public class ClientAccountSrvConfiguration {
    
    public String publishAddress;
    public int publishPort;
    public String[] endpointName;
    
    public ClientAccountSrvConfiguration(String publishAddress, int publishPort, String[] endpointName) {
        this.publishAddress = publishAddress;
        this.publishPort = publishPort;
        this.endpointName = endpointName;
    }
    
    public String getPublishAddress() {
        return publishAddress;
    }

    public void setPublishAddress(String publishAddress) {
        this.publishAddress = publishAddress;
    }

    public int getPublishPort() {
        return publishPort;
    }

    public void setPublishPort(int publishPort) {
        this.publishPort = publishPort;
    }

    public String[] getEndpointName() {
        return endpointName;
    }

    public void setEndpointName(String[] endpointName) {
        this.endpointName = endpointName;
    }
    
    
}
