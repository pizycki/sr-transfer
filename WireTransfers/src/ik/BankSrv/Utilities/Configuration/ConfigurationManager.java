package ik.BankSrv.Utilities.Configuration;

import java.io.FileNotFoundException;
import java.util.IllegalFormatCodePointException;

import ik.BankSrv.ClientAccountService.ClientAccountSrvConfiguration;
import ik.BankSrv.Utilities.Serializer;
import ik.BankSrv.WireTransfers.WireTransfersSrvConfiguration;

/* Singleton */
public class ConfigurationManager {

	public final static boolean CREATE_INIT_CONFIGS = true;

	// ========== <Constructors> ==========
	private ConfigurationManager(boolean fromFile) throws Exception {

		if (fromFile) {
			if (CREATE_INIT_CONFIGS) {
				AppConfiguration config = new AppConfiguration();
				config.setWireTransfersServerConfigurationFilePath("wire-transfers.config");
				config.setClientAccountSrvConfigurationFilePath("client-service.config");
				Serializer.serialize(config, APP_CONFIGURATION_FILE_PATH);
			}

			try {
				configuration = Serializer.deserialize(
						APP_CONFIGURATION_FILE_PATH,
						new AppConfiguration().getClass());
			} catch (FileNotFoundException e) {
				throw new Exception(
						"Error reading configuration file. Application cannot start.");
			}
		} else {
			// Config has been already loaded.
		}

		// Prepare
		prepareWireTransfersSrvConfiguration();
		prepareClientAccountSrvConfiguration();

	}

	// ========== </Constructors> ==========

	// ========== <Getters / Setters> ==========
	public static ConfigurationManager getInstance() {
		if (thisInstance == null) {
			try {
				thisInstance = new ConfigurationManager(true);
			} catch (Exception e) {
				thisInstance = null;
				e.printStackTrace();
			}
		}
		return thisInstance;
	}

	/**
	 * Gets instance
	 * 
	 * @param config
	 * @return
	 */
	public static ConfigurationManager getInstance(AppConfiguration config) {
		if (config == null)
			throw new NullPointerException("Config cannot be null.");

		configuration = config;

		return thisInstance;
	}

	/**
	 * 
	 */
	public WireTransfersSrvConfiguration getWireTransfersSrvConfig() {
		return wireTransfersSrvConfig;
	}

	/**
	 * 
	 */
	public AppConfiguration getConfiguration() {
		return configuration;
	}

	// ========== </Getters / Setters> ==========

	// ========== <Methods> ==========
	/**
	 * @throws Exception
	 * 
	 */
	private void prepareWireTransfersSrvConfiguration() throws Exception {
		if (CREATE_INIT_CONFIGS) {
			String[] endpoints = new String[] { "transfer" };
			this.wireTransfersSrvConfig = new WireTransfersSrvConfiguration(
					"http://localhost", 6789, endpoints);
			Serializer
					.serialize(wireTransfersSrvConfig, 
							configuration.getWireTransfersServerConfigurationFilePath());
		}

		String wireTransferSrvConfigFilePath = configuration
				.getWireTransfersServerConfigurationFilePath();
		try {
			this.wireTransfersSrvConfig = Serializer.deserialize(
					wireTransferSrvConfigFilePath,
					WireTransfersSrvConfiguration.class);
		} catch (FileNotFoundException e) {
			throw new Exception(
					"Error reading configuration file. Application cannot start.");
		}
	}
	
	private void prepareClientAccountSrvConfiguration() throws Exception {
		
		if (CREATE_INIT_CONFIGS) {
			String[] endpoints = new String[] {"client"};
			String publishAddress = "http://localhost";
			int publishPort = 6790;
			
			this.clientAccountSrvConfiguration = new 
					ClientAccountSrvConfiguration(publishAddress, publishPort, endpoints);
			Serializer.serialize(clientAccountSrvConfiguration, configuration.getClientAccountSrvConfigurationFilePath());
		}
		
		String clientAccountSrvConfigurationFilePath = configuration.getClientAccountSrvConfigurationFilePath();
		
		try {
			this.clientAccountSrvConfiguration = Serializer.deserialize(
					clientAccountSrvConfigurationFilePath,
					ClientAccountSrvConfiguration.class);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// ========== </Methods> ==========

	// ========== <Members> ==========
	private static ConfigurationManager thisInstance;
	
	private WireTransfersSrvConfiguration wireTransfersSrvConfig;
	private ClientAccountSrvConfiguration clientAccountSrvConfiguration;

	private static AppConfiguration configuration = null;
	public static final String APP_CONFIGURATION_FILE_PATH = "app.config";

	// ========== </Members> ==========
}
