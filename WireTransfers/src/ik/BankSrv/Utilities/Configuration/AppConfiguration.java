package ik.BankSrv.Utilities.Configuration;

public class AppConfiguration {

	public AppConfiguration() {
		
	}

	private String wireTransfersServerConfigurationFilePath;
	private String clientAccountSrvConfigurationFilePath;

	public String getWireTransfersServerConfigurationFilePath() {
		return wireTransfersServerConfigurationFilePath;
	}

	public void setWireTransfersServerConfigurationFilePath(
			String wireTransfersServerConfigurationFilePath) {
		this.wireTransfersServerConfigurationFilePath = wireTransfersServerConfigurationFilePath;
	}

	public String getClientAccountSrvConfigurationFilePath() {
		return clientAccountSrvConfigurationFilePath;
	}

	public void setClientAccountSrvConfigurationFilePath(
			String clientAccountSrvConfigurationFilePath) {
		this.clientAccountSrvConfigurationFilePath = clientAccountSrvConfigurationFilePath;
	}
}
