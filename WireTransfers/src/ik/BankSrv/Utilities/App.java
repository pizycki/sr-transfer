package ik.BankSrv.Utilities;

import ik.BankSrv.Utilities.Configuration.ConfigurationManager;

public class App {

	/**
	 * Application configuration access point.
	 */
	public static ConfigurationManager AppConfig = ConfigurationManager
			.getInstance();

}
