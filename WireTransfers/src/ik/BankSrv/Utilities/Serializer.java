package ik.BankSrv.Utilities;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

import com.google.gson.Gson;

public class Serializer {

	/**
	 * Retrives object deserializaing it from the file.
	 * 
	 * @param filePath
	 * @return
	 * @throws FileNotFoundException
	 */
	public static <T> T deserialize(String jsonFileLocation, Class<T> clazz)
			throws FileNotFoundException {
		T deserializedObject = null;

		File file = new File(jsonFileLocation);
		try {
                        FileInputStream fis = new FileInputStream(file);
			String fileContent = "";
			int cell;
			while ((cell = fis.read()) != -1) {
				fileContent += (char) cell;
			}
			return convertFrom(clazz, fileContent);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return deserializedObject;
	}

	/**
	 * Puts an object into the file by serialization.
	 * 
	 * @param objectToSerialize
	 * @param xmlFileLocation
	 * @throws FileNotFoundException
	 */
	public static <T> void serialize(T objectToSerialize, String xmlFileLocation)
			throws FileNotFoundException {

		if (objectToSerialize == null) {
			throw new NullPointerException("The object cannot be null!");
		}

		// Convert to JSON
		String objectInJson = convertTo(objectToSerialize);

		// Write to file.
		File outFile = new File(xmlFileLocation);
		try {
                    DataOutputStream outputStream = new DataOutputStream(
				new FileOutputStream(outFile));
			outputStream.writeBytes(objectInJson);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Converts given object into JSON.
	 * 
	 * @param msg
	 * @return
	 */
	public static <T> String convertTo(T msg) {
		return gson.toJson(msg, msg.getClass());
	}

	/**
	 * Converts JSON into object.
	 * 
	 * @param clazz
	 * @param rawJson
	 * @return
	 */
	public static <T> T convertFrom(Class<T> clazz, String rawJson) {
		if (!isValid(rawJson))
			rawJson = fixJson(rawJson);
		return (T) gson.fromJson(rawJson, clazz);
	}

	/**
	 * Checks is the JSON valid
	 * 
	 * @param json
	 * @return true/false
	 */
	public static boolean isValid(String json) {
		byte[] bytes = json.getBytes();
		for (int i = bytes.length - 1; i >= 0; i--) {
			if (bytes[i] == 0)
				return false;
		}
		return true;
	}

	/**
	 * Cuts off the excess leaving plain JSON.
	 * 
	 * @return
	 */
	public static String fixJson(String brokenJson) {
		// Removes all non visible characters
		// DONT USE String.replaceAll - IT WON'T WORK
		byte[] bytes = brokenJson.getBytes();
		int offset = 0;
		for (int i = 0; i < bytes.length; i++)
			if (bytes[i] == 0) {
				offset = i;
				break;
			}

		return new String(Arrays.copyOfRange(bytes, 0, offset));
	}

	protected final static Gson gson = new Gson();
}
