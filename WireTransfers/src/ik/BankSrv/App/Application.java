package ik.BankSrv.App;

import ik.BankSrv.ClientAccountService.ClientAccountSrv;
import ik.BankSrv.FinancialAccounts.FinancialAccountSrv;
import ik.BankSrv.Utilities.App;
import ik.BankSrv.Utilities.Configuration.ConfigurationManager;
import ik.BankSrv.WireTransfers.WireTransfersSrv;
import ik.BankSrv.WireTransfers.Distributor.DistributorConfigManager;
import ik.BankSrv.WireTransfers.Distributor.WireTransfersDistributor;

public class Application {

	public Application() {
		Initialize();
	}

	protected void Initialize() {
		try {
			DistributorConfigManager wtDistConfigMngr = new DistributorConfigManager();
			this.wireTransfersDistributor = new WireTransfersDistributor(
					wtDistConfigMngr);

			wireTransfersSrv = new WireTransfersSrv(
					App.AppConfig.getWireTransfersSrvConfig(),
					wireTransfersDistributor);
                        
            this.clientAccountSrv = new ClientAccountSrv();
            this.financialAccountSrv = new FinancialAccountSrv();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private WireTransfersSrv wireTransfersSrv;
    private ClientAccountSrv clientAccountSrv;
    private FinancialAccountSrv financialAccountSrv;
	private WireTransfersDistributor wireTransfersDistributor;
}
