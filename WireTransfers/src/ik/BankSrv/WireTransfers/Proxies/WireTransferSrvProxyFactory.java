package ik.BankSrv.WireTransfers.Proxies;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import ik.BankSrv.Contracts.IWireTransfersSrv;
import ik.BankSrv.WireTransfers.Distributor.BankWebSrvDetails;

public class WireTransferSrvProxyFactory {

	public static IWireTransfersSrv createProxy(BankWebSrvDetails wsDetails)
			throws NullPointerException {
		if (wsDetails == null)
			throw new NullPointerException("Connection cannot be null.");

		IWireTransfersSrv proxy;
		try {
			proxy = new WireTransfersSrvProxy(wsDetails);
		} catch (MalformedURLException e) {
			proxy = null;
			e.printStackTrace();
		}
		return proxy;
	}

	private static class WireTransfersSrvProxy implements IWireTransfersSrv {

		/**
		 * 
		 */
		final IWireTransfersSrv srv;

		public WireTransfersSrvProxy(BankWebSrvDetails wsDetails)
				throws MalformedURLException {

			String address = wsDetails.getAddress();
			int port = wsDetails.getPort();
			String fullAdress = address + ":" + port + "/ws/transfer?wsdl";
			URL url = new URL(fullAdress);

			String namespace = wsDetails.getNamespace();
			String localPart = wsDetails.getLocalPart();
			QName qName = new QName(namespace, localPart);

			Service service = Service.create(url, qName);
			this.srv = service.getPort(IWireTransfersSrv.class);

			System.out.println("Klient dla " + fullAdress + " jest online!");
		}

		@Override
		public boolean transfer(String accountNumber, double amount) {
			return srv.transfer(accountNumber, amount);
		}
	}
}
