package ik.BankSrv.WireTransfers.Distributor;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

import ik.BankSrv.Utilities.App;
import ik.BankSrv.Utilities.Serializer;

public class DistributorConfigManager {

	public DistributorConfig generateConfig() {

		DistributorConfig distConfig = null;

		if (App.AppConfig.CREATE_INIT_CONFIGS) {
			prepareInitConfig();
		}

		try {
			distConfig = Serializer.deserialize(FILE_PATH,
					new DistributorConfig().getClass());
		} catch (FileNotFoundException e) {
			return null;
		}
		return distConfig;
	}

	private void prepareInitConfig() {
		BankWebSrvDetails wsBank1 = new BankWebSrvDetails();
		wsBank1.setBankPrefix("123");
		wsBank1.setNamespace("http://Mock/");
		wsBank1.setLocalPart("WireTransfersWebServiceService");
		wsBank1.setAddress("http://localhost");
		wsBank1.setPort(5555);

		BankWebSrvDetails wsBank2 = new BankWebSrvDetails();
		wsBank2.setBankPrefix("234");
		wsBank2.setNamespace("http://Mock/");
		wsBank2.setLocalPart("WireTransfersWebServiceService");
		wsBank2.setAddress("http://localhost");
		wsBank2.setPort(5556);

		Set<BankWebSrvDetails> connections = new HashSet<BankWebSrvDetails>();
		connections.add(wsBank1);
		connections.add(wsBank2);

		DistributorConfig config = new DistributorConfig(connections);

		try {
			Serializer.serialize(config, DistributorConfigManager.FILE_PATH);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static final String FILE_PATH = "distributor.config";

}
