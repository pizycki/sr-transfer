package ik.BankSrv.WireTransfers.Distributor;

import ik.BankSrv.Contracts.IWireTransfersSrv;
import ik.BankSrv.WireTransfers.Proxies.WireTransferSrvProxyFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class WireTransfersDistributor {

	private DistributorConfigManager distConfigMngr;
	private Map<String, IWireTransfersSrv> wireTransferClients;

	public WireTransfersDistributor(DistributorConfigManager distConfigMngr) {
		this.distConfigMngr = distConfigMngr;

		Init();
	}

	public void Init() {
		// Prepare clients
		try {
			prepareClients();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void prepareClients() throws Exception {
		if (distConfigMngr == null)
			throw new Exception("Configuration manager is null.");

		DistributorConfig config = distConfigMngr.generateConfig();
		Set<BankWebSrvDetails> wsDetailsSet = config.BankWebSrvDetailsSet;
		if (this.wireTransferClients == null) {
			this.wireTransferClients = new HashMap<String, IWireTransfersSrv>();
		} else {
			this.wireTransferClients.clear();
		}
		for (BankWebSrvDetails bankWebSrvDetails : wsDetailsSet) {
			IWireTransfersSrv bankWebSrv = WireTransferSrvProxyFactory
					.createProxy(bankWebSrvDetails);

			if (bankWebSrv != null) {
				this.wireTransferClients.put(bankWebSrvDetails.getBankPrefix(),
						bankWebSrv);
			}
		}
	}
}
