package ik.BankSrv.WireTransfers.Distributor;

import java.io.Serializable;
import java.util.Set;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DistributorConfig implements Serializable {

	@XmlElement
	public Set<BankWebSrvDetails> BankWebSrvDetailsSet;

	// ========== <Constructors> ==========

	public DistributorConfig() {
	}

	public DistributorConfig(Set<BankWebSrvDetails> connections) {
		this.BankWebSrvDetailsSet = connections;
	}

	// ========== </Constructors> ==========
}
