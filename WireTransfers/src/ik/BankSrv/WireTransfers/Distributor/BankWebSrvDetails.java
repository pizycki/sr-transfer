package ik.BankSrv.WireTransfers.Distributor;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BankWebSrvDetails {

	// ========== <Members> ==========

	@XmlAttribute
	private String bankPrefix;

	@XmlAttribute
	private String address;

	@XmlAttribute
	private int port;

	@XmlAttribute
	private String namespace;

	@XmlAttribute
	private String localPart;

	// ========== </Members> ==========

	// ========== <Getters / Setters> ==========

	public String getBankPrefix() {
		return bankPrefix;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getLocalPart() {
		return localPart;
	}

	public void setLocalPart(String localPart) {
		this.localPart = localPart;
	}

	public void setBankPrefix(String bankID) {
		this.bankPrefix = bankID;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	// ========== </Getters / Setters> ==========
}
