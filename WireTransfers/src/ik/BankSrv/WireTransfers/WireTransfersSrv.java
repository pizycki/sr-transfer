package ik.BankSrv.WireTransfers;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;

import ik.BankSrv.Contracts.IWireTransfersSrv;
import ik.BankSrv.WireTransfers.Distributor.WireTransfersDistributor;

@WebService(endpointInterface = "ik.BankSrv.Contracts.IWireTransfersSrv")
public class WireTransfersSrv implements IWireTransfersSrv {

	// ========== <Events> ==========

	// ========== </Events> ==========

	// ========== <Getters / Setters> ==========

	// ========== </Getters / Setters> ==========

	// ========== <Methods> ==========
	public void init() {
		// Open endpoints for web service
		openEndpoints();
	}

	protected void openEndpoints() {
		String fullPublishAddress = configuration.getPublishAddress() + ":"
				+ configuration.getPublishPort() + "/ws/";
		String[] endpointNames = configuration.getEndpointNames();
		for (String endpointName : endpointNames) {
			Endpoint.publish(fullPublishAddress, this);
		}
	}

	@Override
	public boolean transfer(String accountNumber, double amount) {
		// TODO implement
		return false;
	}

	// ========== </Methods> ==========

	// ========== <Constructors> ==========
	public WireTransfersSrv(WireTransfersSrvConfiguration configuration,
			WireTransfersDistributor wireTransfersDistributor) {		
		this.wireTransfersDistributor = wireTransfersDistributor;
		this.configuration = configuration;
		
		// Initialize
		init();
	}

	public WireTransfersSrv() {
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	private WireTransfersDistributor wireTransfersDistributor;
	private WireTransfersSrvConfiguration configuration;
	// ========== </Members> ==========

}
