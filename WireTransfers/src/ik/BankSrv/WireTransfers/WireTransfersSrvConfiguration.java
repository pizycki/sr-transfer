package ik.BankSrv.WireTransfers;

import java.io.Serializable;

public class WireTransfersSrvConfiguration implements Serializable {

	// ========== <Constructors> ==========
	public WireTransfersSrvConfiguration() {
	}

	public WireTransfersSrvConfiguration(String publishAddress,
			int publishPort, String[] endpointNames) {
		this.publishAddress = publishAddress;
		this.publishPort = publishPort;
		this.endpointNames = endpointNames;
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	private String publishAddress;
	private int publishPort;
	private String[] endpointNames;

	// ========== </Members> ==========

	// ========== <Getters / Setters> ==========
	public String getPublishAddress() {
		return publishAddress;
	}

	public void setPublishAddress(String publishAddress) {
		this.publishAddress = publishAddress;
	}

	public int getPublishPort() {
		return publishPort;
	}

	public void setPublishPort(int publishPort) {
		this.publishPort = publishPort;
	}

	public String[] getEndpointNames() {
		return endpointNames;
	}

	public void setEndpointNames(String[] endpointNames) {
		this.endpointNames = endpointNames;
	}
	// ========== </Getters / Setters> ==========

}
