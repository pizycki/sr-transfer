## Numer konta bankowego ##

Składa się z sześciu cyfr, od 0 do 9, gdzie pierwsze cztery cyfry są takie same jak port na którym stoi server, pozostałe dwie to liczby naturalne oznaczające id konta, np: Numer konta *678901* oznacza, że serwer stoi na porcie *6789*, a id konta to *01*.

## Dokonywanie przelewu ##

Podczas dokonywania przelewu sprawdzane są numery rachunków nadawcy jak i odbiorcy.
Komunikacja

Komunikacja pomiędzy serwer-serwer oraz serwer-terminal oparta jest o protokół TCP. Przyjęte zostało, że port komunikacji pomiędzy serwerami bankowymi jest następnym portem po komunikacji między serwerem a terminalem, np.: jeżeli serwer wystawia usługę dla terminalu na porcie 5555 to jego usługi wystawione dla innych banków są wystawione na porcie 5556.