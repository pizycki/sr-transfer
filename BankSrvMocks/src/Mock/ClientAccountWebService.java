package Mock;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;

import ik.BankSrv.Contracts.IClientAccountSrv;

@WebService(endpointInterface = "ik.BankSrv.Contracts.IClientAccountSrv")
public class ClientAccountWebService implements IClientAccountSrv {

	public ClientAccountWebService(int port){
		String address = "http://localhost:" + port + "/ws/client";
		Endpoint.publish(address, this);
		System.out.println("WebService na adresie: " + address
				+ " jest online!");
	}
	
	@Override
	public Double checkBalance(String accountNumber){
		Double balance = 1.0;
		
		return balance;
	}

}
