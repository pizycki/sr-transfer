package Mock;

import ik.BankSrv.Contracts.IWireTransfersSrv;

import java.util.*;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService(endpointInterface = "ik.BankSrv.Contracts.IWireTransfersSrv")
public class WireTransfersWebService implements IWireTransfersSrv {

	public WireTransfersWebService(int port) {
		String address = "http://localhost:" + port + "/ws/transfer";
		Endpoint.publish(address, this);
		System.out.println("WebService na adresie: " + address
				+ " jest online!");
	}

	@Override
	public boolean transfer(String accountNumber, double amount) {
		System.out.println("Czas: " + new Date().toLocaleString()
				+ ", nadeszło zgłoszenie o transfer");
		return false;
	}
}
